//
//  UIImageView+Extension.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/15/21.
//

import SDWebImage
import UIKit

extension UIImageView {
    
    func setImage(with url: String, placeHolderImage name: String, completion: ((UIImage) -> Void)? = nil) {
        self.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: name), options: .highPriority, completed: nil)
    }
}
