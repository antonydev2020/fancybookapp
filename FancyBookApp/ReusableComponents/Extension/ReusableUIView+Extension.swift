//
//  ReusableUIView+Extension.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/15/21.
//

import UIKit

protocol ReusableUIView {
    static var reuseIdentifier: String { get }
}

extension ReusableUIView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
