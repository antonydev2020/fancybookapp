//
//  BookService.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/24/21.
//

import Foundation

enum BookService {
    case volumeSearch(String, resultLimit: Int, pageNum: Int)
    case userBookShevles(String, Int)
}

extension BookService: RequestProtocol {
    
    var baseUrl: String {
        "https://www.googleapis.com/"
    }
    
    var path: String {
        switch self {
        case .volumeSearch:
            return "books/v1/volumes"
        case .userBookShevles(let userId, let shelves):
            return "books/v1/users/\(userId)/bookshelves/\(shelves)/volumes"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .volumeSearch, .userBookShevles:
            return .get
        }
    }
    
    var queryParams: RequestParameters? {
        switch self {
        case .volumeSearch(let searchText, let resultLimit, let pageNumber):
            return [
                "q": "\(searchText)",
                "maxResults": resultLimit,
                "startIndex": pageNumber
            ]
            
        case .userBookShevles:
            return nil
        }
    }
}
