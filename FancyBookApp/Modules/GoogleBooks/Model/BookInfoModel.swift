//
//  BookInfoModel.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/16/21.
//

// check week7 playground for my implementation

import Foundation

struct Item: Codable {
    var kind: Kind?
    var id, etag: String?
    var selfLink: String?
    var volumeInfo: VolumeInfo?
    var saleInfo: SaleInfo?
    var accessInfo: AccessInfo?
    var searchInfo: SearchInfo?
}

// swiftlint:disable discouraged_optional_boolean

struct AccessInfo: Codable {
    var country: Country?
    var viewability: Viewability?
    var embeddable, publicDomain: Bool?
    var textToSpeechPermission: TextToSpeechPermission?
    var epub, pdf: Epub?
    var webReaderLink: String?
    var accessViewStatus: AccessViewStatus?
    var quoteSharingAllowed: Bool?
}

enum AccessViewStatus: String, Codable {
    case none = "NONE"
    case sample = "SAMPLE"
}

enum Country: String, Codable {
    case us = "US"
}

struct Epub: Codable {
    var isAvailable: Bool?
    var acsTokenLink: String?
}

enum TextToSpeechPermission: String, Codable {
    case allowed = "ALLOWED"
    case allowedForAccessibility = "ALLOWED_FOR_ACCESSIBILITY"
}

enum Viewability: String, Codable {
    case noPages = "NO_PAGES"
    case partial = "PARTIAL"
}

enum Kind: String, Codable {
    case booksVolume = "books#volume"
}

struct SaleInfo: Codable {
    var country: Country?
    var saleability: Saleability?
    var isEbook: Bool?
    var listPrice, retailPrice: SaleInfoListPrice?
    var buyLink: String?
    var offers: [Offer]?
}

struct SaleInfoListPrice: Codable {
    var amount: Double?
    var currencyCode: CurrencyCode?
}

enum CurrencyCode: String, Codable {
    case usd = "USD"
}

struct Offer: Codable {
    var finskyOfferType: Int?
    var listPrice, retailPrice: OfferListPrice?
    var giftable: Bool?
}

struct OfferListPrice: Codable {
    var amountInMicros: Int?
    var currencyCode: CurrencyCode?
}

enum Saleability: String, Codable {
    case forSale = "FOR_SALE"
    case notForSale = "NOT_FOR_SALE"
}

struct SearchInfo: Codable {
    var textSnippet: String?
}

struct VolumeInfo: Codable {
    var title: String?
    var authors: [String]?
    var publisher, publishedDate, volumeInfoDescription: String?
    var industryIdentifiers: [IndustryIdentifier]?
    var readingModes: ReadingModes?
    var pageCount: Int?
    var printType: PrintType?
    var categories: [Category]?
    var averageRating, ratingsCount: Int?
    var maturityRating: MaturityRating?
    var allowAnonLogging: Bool?
    var contentVersion: String?
    var panelizationSummary: PanelizationSummary?
    var imageLinks: ImageLinks?
    var language: Language?
    var previewLink: String?
    var infoLink: String?
    var canonicalVolumeLink: String?
    var subtitle: String?

    enum CodingKeys: String, CodingKey {
        case title, authors, publisher, publishedDate
        case volumeInfoDescription = "description"
        case industryIdentifiers, readingModes, pageCount, printType, categories, averageRating
        case ratingsCount, maturityRating, allowAnonLogging, contentVersion, panelizationSummary, imageLinks
        case language, previewLink, infoLink, canonicalVolumeLink, subtitle
    }
}

enum Category: String, Codable {
    case computerSoftware = "Computer software"
    case computers = "Computers"
}

struct ImageLinks: Codable {
    var smallThumbnail, thumbnail: String?
}

struct IndustryIdentifier: Codable {
    var type: TypeEnum?
    var identifier: String?
}

enum TypeEnum: String, Codable {
    case isbn10 = "ISBN_10"
    case isbn13 = "ISBN_13"
}

enum Language: String, Codable {
    case en
}

enum MaturityRating: String, Codable {
    case notMature = "NOT_MATURE"
}

struct PanelizationSummary: Codable {
    var containsEpubBubbles, containsImageBubbles: Bool?
}

enum PrintType: String, Codable {
    case book = "BOOK"
}

struct ReadingModes: Codable {
    var text, image: Bool?
}

// swiftlint:enable discouraged_optional_boolean

struct BookInfoModel: Codable {
    var kind: String?
    var totalItems: Int?
    var items: [Item]?
}
