//
//  ListViewModel.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/11/22.
//

import Foundation

protocol ListViewModelDelegate: AnyObject {
    func fetchSucceeded()
    func didFail(with error: QueryError)
}

enum QueryOperations {
    case booksFetched
    case failed(QueryError)
}

protocol ListViewModelProtocol: AnyObject {
    var listViewDelegate: ListViewModelDelegate? { get set }
    var notifier: ((QueryOperations) -> Void)? { get set }
    
    var nextPage: Int { get }
    var isNextAvailable: Bool { get set }
    
    init(networkProvider: NetworkProvider)
    func fetch(endPoint: BookService)
    func books(at index: Int) -> Item
    func numberOfBooks(in section: Int) -> Int
    func getBooks(searchText: String, pageResults: Int, startPage: Int)
}

class ListViewModel: ListViewModelProtocol {
    
    private var bookDataSource: [Item] = []
    private var networkProvider: NetworkProvider
    
    weak var listViewDelegate: ListViewModelDelegate?
    var notifier: ((QueryOperations) -> Void)?
    
    var nextPage: Int {
        bookDataSource.count
    }
    
    var isNextAvailable = true
    
    required init(networkProvider: NetworkProvider = NetworkManager(session: URLSession.shared)) {
        self.networkProvider = networkProvider
    }
    
    func books(at index: Int) -> Item {
        self.bookDataSource[index]
    }
    
    func numberOfBooks(in section: Int) -> Int {
        self.bookDataSource.count
    }
    
    func getBooks(searchText: String, pageResults: Int = 20, startPage: Int = 0) {
        fetch(endPoint: .volumeSearch(searchText, resultLimit: pageResults, pageNum: startPage))
    }
    
    func fetch(endPoint: BookService) {
        networkProvider.fetch(request: endPoint) { [weak self] (result: Result<BookInfoModel, QueryError>) in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                self.bookDataSource.append(contentsOf: model.items ?? [])
                print(model.totalItems)
                print(self.bookDataSource.count)
                self.isNextAvailable = model.totalItems ?? 0 > self.bookDataSource.count
                
                self.listViewDelegate?.fetchSucceeded()
                self.notifier?(.booksFetched)
                
            case .failure(let error):
                self.listViewDelegate?.didFail(with: error)
                self.notifier?(.failed(error))
            }
        }
    }
}
