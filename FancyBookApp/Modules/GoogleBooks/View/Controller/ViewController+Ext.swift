//
//  ViewController+Ext.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/11/22.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfBooks(in: section)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.numberOfBooks(in: indexPath.section) - 1 {
            self.search()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableCell = tableView.dequeueReusableCell(withIdentifier: FancyBookCell.reuseIdentifier, for: indexPath) as? FancyBookCell else {
            fatalError("Unable to dequeue cell with identifier \(FancyBookCell.reuseIdentifier)")
        }
        
        let model = viewModel.books(at: indexPath.row)
        
        let getBookImage = model.volumeInfo?.imageLinks?.thumbnail
        let getTitle = model.volumeInfo?.title ?? ""
        let getAuthor = model.volumeInfo?.authors?.joined(separator: ", ") ?? ""
        let getRating = model.volumeInfo?.averageRating ?? 0
        //        let getDate = model.volumeInfo?.publishedDate ?? ""
        
        tableCell.bookCellConfigure(thumbnailUrl: getBookImage,
                                    title: getTitle,
                                    author: getAuthor,
                                    rating: "\(getRating)")
        
        return tableCell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            //            let numberOfCell: CGFloat = 1
            let spacing = layout.minimumInteritemSpacing + layout.sectionInset.left + layout.sectionInset.right
            let width = collectionView.frame.width
            let height = collectionView.frame.height - spacing
            return CGSize(width: width, height: height)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == viewModel.numberOfBooks(in: indexPath.section) - 1 {
            self.search()
        }
    }
}

extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfBooks(in: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: BookCarouselCell.reuseIdentifier, for: indexPath) as? BookCarouselCell else {
            fatalError("Unable to dequeue cell with identifier \(BookCarouselCell.reuseIdentifier)")
        }
        
        let model = viewModel.books(at: indexPath.row)
        let getBookThumbnail = model.volumeInfo?.imageLinks?.thumbnail
        
        collectionCell.carouselConfigure(bookThumbnail: getBookThumbnail)
        
        return collectionCell
    }
}
