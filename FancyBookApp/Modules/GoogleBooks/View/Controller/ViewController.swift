//
//  ViewController.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/15/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(UINib(nibName: FancyBookCell.reuseIdentifier, bundle: Bundle.main), forCellReuseIdentifier: FancyBookCell.reuseIdentifier)
        }
    }
    
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(UINib(nibName: BookCarouselCell.reuseIdentifier, bundle: Bundle.main), forCellWithReuseIdentifier: BookCarouselCell.reuseIdentifier)
        }
    }
    
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    var viewModel: ListViewModelProtocol = ListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setUp()
    }
    
    private func setUp() {
        viewModel.notifier = { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .booksFetched:
                self.tableView.reloadData()
                self.collectionView.reloadData()
                
            case .failed(let error):
                print(error)
            }
        }
    }
    
    private func getBooks(searchText: String) {
        guard viewModel.isNextAvailable else { return }
        viewModel.getBooks(searchText: searchText, pageResults: 20, startPage: viewModel.nextPage)
    }
}

extension ViewController: ListViewModelDelegate {
    func fetchSucceeded() {
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
    
    func didFail(with error: QueryError) {
        // Handle the error here
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search()
    }
    
    func search() {
        guard let searchText = searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
              !searchText.isEmpty else { return }
        getBooks(searchText: searchText)
    }
}
