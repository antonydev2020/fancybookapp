//
//  BookCarouselCell.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/17/21.
//

import UIKit

class BookCarouselCell: UICollectionViewCell, ReusableUIView {
    
    @IBOutlet private weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 12
            containerView.layer.borderWidth = 2
            containerView.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    @IBOutlet private weak var bookThumbnailView: UIImageView! {
        didSet {
            bookThumbnailView.layer.cornerRadius = 20
            bookThumbnailView.layer.borderWidth = 1
            bookThumbnailView.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    func carouselConfigure(bookThumbnail: String?) {
        if let imageUrl = bookThumbnail {
            self.bookThumbnailView.setImage(with: imageUrl, placeHolderImage: "books-placeholder")
        }
    }
}
