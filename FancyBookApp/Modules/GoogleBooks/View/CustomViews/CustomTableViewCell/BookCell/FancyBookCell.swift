//
//  FancyBookCell.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/15/21.
//

import UIKit

class FancyBookCell: UITableViewCell, ReusableUIView {
    
    @IBOutlet private weak var containerView: UIView! {
        didSet {
            containerView.layer.shadowOpacity = 0.18
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowRadius = 2
            containerView.layer.shadowColor = UIColor.black.cgColor
            containerView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet private weak var bookThumbnailView: UIImageView! {
        didSet {
            bookThumbnailView.layer.cornerRadius = 12
            bookThumbnailView.layer.borderWidth = 2
            bookThumbnailView.layer.borderColor = UIColor.gray.cgColor

            bookThumbnailView.layer.shadowOpacity = 0.18
            bookThumbnailView.layer.shadowOffset = CGSize(width: 0, height: 2)
            bookThumbnailView.layer.shadowRadius = 2
            bookThumbnailView.layer.shadowColor = UIColor.black.cgColor
            bookThumbnailView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    
    func bookCellConfigure(thumbnailUrl: String?,
                           title: String,
                           author: String,
                           rating: String) {
        
        self.titleLabel.text = title
        self.authorLabel.text = author
        self.ratingLabel.text = rating
        
        if let imageUrl = thumbnailUrl {
            self.bookThumbnailView.setImage(with: imageUrl, placeHolderImage: "books-placeholder")
        }
    }
}
