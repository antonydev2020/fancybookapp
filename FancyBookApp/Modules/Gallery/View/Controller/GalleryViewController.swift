//
//  GalleryViewController.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/10/22.
//

import UIKit

class GalleryViewController: UIViewController {
    
    @IBOutlet private weak var galleryCollectionView: UICollectionView! {
        didSet {
            galleryCollectionView.dataSource = self
            galleryCollectionView.delegate = self
            galleryCollectionView.register(UINib(nibName: GalleryCollectionViewCell.reuseIdentifier, bundle: Bundle.main), forCellWithReuseIdentifier: GalleryCollectionViewCell.reuseIdentifier)
            flowLayout()
        }
    }
    
    var galleryViewModel: GalleryViewModelProtocol = GalleryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryViewModel.galleryViewDelegate = self
        self.getGallery()

        // Do any additional setup after loading the view.
    }
    
    private func getGallery() {
        galleryViewModel.fetch(endPoint: .list(pageNum: 1, limitNum: 30))
    }
    
    private func flowLayout() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        flowLayout.scrollDirection = .vertical
        galleryCollectionView.collectionViewLayout = flowLayout
    }
}

extension GalleryViewController: GalleryViewModelDelegate {
    func fetchSucceeded() {
        self.galleryCollectionView.reloadData()
    }
    
    func didFail(with error: QueryError) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default) { _ in }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
