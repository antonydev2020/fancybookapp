//
//  GalleryViewController+Ext.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/12/22.
//

import Foundation
import UIKit

extension GalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryViewModel.numberOfPhotos(in: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reUse: String = GalleryCollectionViewCell.reuseIdentifier
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reUse, for: indexPath) as? GalleryCollectionViewCell else { fatalError("Unable to dq cell with identifier \(reUse)") }
        
        let model = galleryViewModel.photos(at: indexPath.row)
        
        let getImageUrl = model.downloadURL
        let getPhotographer = model.author ?? ""
        
        cell.galleryConfigure(imageUrl: getImageUrl, photographer: getPhotographer)
        return cell
    }
}

extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            let numberOfCells: CGFloat = 2
            let spacing = layout.minimumInteritemSpacing + layout.sectionInset.left + layout.sectionInset.right
            let width = (collectionView.frame.width - spacing) / numberOfCells
            let height = (collectionView.frame.height / 3) - (layout.minimumLineSpacing + layout.sectionInset.top + layout.sectionInset.bottom)
            return CGSize(width: width, height: height)
        }
        return .zero
    }
}
