//
//  GalleryCollectionViewCell.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/10/22.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell, ReusableUIView {
    
    @IBOutlet private weak var galleryImage: UIImageView!
    @IBOutlet private weak var imageAuthorLabel: UILabel!
    
    func galleryConfigure(imageUrl: String?, photographer: String) {
        if let url = imageUrl {
            galleryImage.setImage(with: url, placeHolderImage: "galleryPlaceholder")
        }
        
        self.imageAuthorLabel.text = photographer
    }
}
