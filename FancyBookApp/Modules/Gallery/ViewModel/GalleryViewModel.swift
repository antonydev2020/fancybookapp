//
//  GalleryViewModel.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/12/22.
//

import Foundation

protocol GalleryViewModelDelegate: AnyObject {
    func fetchSucceeded()
    func didFail(with error: QueryError)
}

protocol GalleryViewModelProtocol: AnyObject {
    var galleryViewDelegate: GalleryViewModelDelegate? { get set }
    
    init(networkProvider: NetworkProvider)
    func fetch(endPoint: GalleryServices)
    func photos(at index: Int) -> GalleryInfo
    func numberOfPhotos(in section: Int) -> Int
}

class GalleryViewModel: GalleryViewModelProtocol {
    
    private var galleryDataSource: [GalleryInfo] = []
    private var networkProvider: NetworkProvider
    
    weak var galleryViewDelegate: GalleryViewModelDelegate?
    
    required init(networkProvider: NetworkProvider = NetworkManager(session: URLSession.shared)) {
        self.networkProvider = networkProvider
    }
    
    func fetch(endPoint: GalleryServices) {
        networkProvider.fetch(request: endPoint) { [weak self] (result: Result<[GalleryInfo], QueryError>) in
            guard let self = self else { return }
            switch result {
            case .success(let image):
                self.galleryDataSource = image
                self.galleryViewDelegate?.fetchSucceeded()
                
            case .failure(let error):
                self.galleryViewDelegate?.didFail(with: error)
            }
        }
    }
    
    func photos(at index: Int) -> GalleryInfo {
        self.galleryDataSource[index]
    }
    
    func numberOfPhotos(in section: Int) -> Int {
        self.galleryDataSource.count
    }
}
