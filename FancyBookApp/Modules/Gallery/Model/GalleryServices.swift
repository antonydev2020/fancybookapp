//
//  GalleryServices.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/10/22.
//

import Foundation

enum GalleryServices {
    case list(pageNum: Int, limitNum: Int)
}

extension GalleryServices: RequestProtocol {
    var baseUrl: String {
        "https://picsum.photos/"
    }
    
    var path: String {
        "v2/list"
    }
    
    var method: HTTPMethod {
        switch self {
        case .list:
            return .get
        }
    }
    
    var queryParams: RequestParameters? {
        switch self {
        case .list(let pageNumber, let limit):
            return [
                "page": pageNumber,
                "limit": limit
            ]
        }
    }
}
