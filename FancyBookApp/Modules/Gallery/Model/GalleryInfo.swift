//
//  GalleryInfo.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 1/10/22.
//

import Foundation

struct GalleryInfo: Codable {
    let id, author: String?
    let width, height: Int?
    let url, downloadURL: String?

    enum CodingKeys: String, CodingKey {
        case id, author, width, height, url
        case downloadURL = "download_url"
    }
}
