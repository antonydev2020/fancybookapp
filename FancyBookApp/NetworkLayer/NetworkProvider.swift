//
//  NetworkProvider.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/23/21.
//

import Foundation

typealias RequestHeaders = [String: String]
typealias RequestParameters = [String: Any]

protocol RequestProtocol {
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: RequestHeaders? { get }
    var bodyParams: RequestParameters? { get }
    var queryParams: RequestParameters? { get }
}

protocol NetworkSessionProtocol {
    func dataTask(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

protocol NetworkProvider {
    var session: NetworkSessionProtocol { get }
    
    init(session: NetworkSessionProtocol)
    func fetch<T: Decodable>(request: RequestProtocol, completionHandler: @escaping (Result<T, QueryError>) -> Void)
}

extension RequestProtocol {
    var baseUrl: String {
        ""
    }
    
    var headers: RequestHeaders? {
        return nil
    }
    
    var bodyParams: RequestParameters? {
        return nil
    }
    
    var queryParams: RequestParameters? {
        return nil
    }
    
    func asUrlRequest() -> URLRequest? {
        guard let url = self.url(with: baseUrl) else { return nil }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpBody = jsonBody
        return urlRequest
    }
    
    private func url(with baseUrl: String) -> URL? {
        guard var components = URLComponents(string: baseUrl) else { return nil }
        components.path = "\(components.path)\(path)"
        components.queryItems = queryItems
        print(components)
        return components.url
    }
    
    private var jsonBody: Data? {
        guard [.post, .put, .patch].contains(method),
              let params = bodyParams else { return nil }
        do {
            let data = try JSONSerialization.data(withJSONObject: params, options: .fragmentsAllowed)
            return data
        } catch {
            print(error)
            return nil
        }
    }
    
    private var queryItems: [URLQueryItem]? {
        guard method == .get, let queryItems = queryParams else { return nil }
        return queryItems.compactMap { (key: String, value: Any) -> URLQueryItem in
            let valueString = "\(value)"
            return URLQueryItem(name: key, value: valueString)
        }
    }
}
