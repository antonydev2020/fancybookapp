//
//  NetworkManager.swift
//  FancyBookApp
//
//  Created by Antony Alexander Mylvaganam on 12/15/21.
//

import Foundation

enum QueryError: Error {
    case badUrl
    case networkError
    case badResponse
    case noData
    case cannotParse
    case serverError
}

enum HTTPMethod: String {
    case get
    case post
    case put
    case patch
    case delete
}

class NetworkManager: NetworkProvider {
    
    var session: NetworkSessionProtocol
    
    required init(session: NetworkSessionProtocol) {
        self.session = session
    }
    
    func fetch<T: Decodable>(request: RequestProtocol, completionHandler: @escaping (Result<T, QueryError>) -> Void) {
        
        guard let finalRequest = request.asUrlRequest() else {
            completionHandler(.failure(.badUrl))
            return
        }

        session.dataTask(request: finalRequest) { data, response, error in
            let completionOnMain: (Result) -> Void = { result in
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            }
//            networkError
            guard error == nil else {
                completionOnMain(.failure(.networkError))
                return
            }
//            badResponse
            guard let response = response  as? HTTPURLResponse else {
                completionOnMain(.failure(.badResponse))
                return
            }
//            noData
            switch response.statusCode {
            case 200...209:
                guard let dataUnWrapped = data else {
                    completionOnMain(.failure(.noData))
                    return
                }
                do {
                    let convertToModel = try JSONDecoder().decode(T.self, from: dataUnWrapped)
                    completionOnMain(.success(convertToModel))
                } catch {
//                    cannnot parse
                    completionOnMain(.failure(.cannotParse))
                }
                
            default:
//                serverError
                completionOnMain(.failure(.serverError))
            }
        }
    }
}

extension URLSession: NetworkSessionProtocol {
    func dataTask(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = dataTask(with: request, completionHandler: completion)
        task.resume()
    }
}
